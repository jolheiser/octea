package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"code.gitea.io/octea/cron"
	"code.gitea.io/octea/router"

	"github.com/google/go-github/v48/github"
	"github.com/peterbourgon/ff/v3"
	"github.com/peterbourgon/ff/v3/fftoml"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
)

func main() {
	fs := flag.NewFlagSet("octea", flag.ContinueOnError)
	portFlag := fs.Int("port", 8080, "Port to listen on")
	jsonFlag := fs.Bool("json", false, "JSON logging")
	secretFlag := fs.String("secret", "", "Webhook secret")
	tokenFlag := fs.String("token", "", "GitHub token")
	logLevel := zerolog.InfoLevel
	fs.Func("log-level", "Set logging level (default: info)", func(s string) error {
		lvl, err := zerolog.ParseLevel(s)
		if err != nil {
			return err
		}
		logLevel = lvl
		return nil
	})
	if err := ff.Parse(fs, os.Args[1:],
		ff.WithEnvVarPrefix("OCTEA"),
		ff.WithAllowMissingConfigFile(true),
		ff.WithConfigFileFlag("config"),
		ff.WithConfigFileParser(fftoml.New().Parse),
	); err != nil {
		log.Err(err).Msg("")
		return
	}

	zerolog.SetGlobalLevel(logLevel)
	if !*jsonFlag {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	ts := oauth2.StaticTokenSource(&oauth2.Token{
		AccessToken: *tokenFlag,
	})
	tc := oauth2.NewClient(context.Background(), ts)
	client := github.NewClient(tc)

	cron.New(client).Start()

	go func() {
		r := router.New([]byte(*secretFlag), client)
		port := fmt.Sprintf(":%d", *portFlag)
		log.Debug().Msgf("listening at http://localhost%s", port)
		if err := http.ListenAndServe(port, r); err != nil {
			log.Err(err).Msg("could not serve http")
		}
	}()

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt, os.Kill)
	<-ch
}
