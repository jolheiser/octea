package cron

import (
	"time"

	"github.com/google/go-github/v48/github"
	"github.com/rs/zerolog/log"
)

var jobs map[string]job

type job struct {
	interval time.Duration
	fn       func(client *github.Client) error
}

type cron struct {
	client *github.Client
}

func New(client *github.Client) *cron {
	return &cron{
		client: client,
	}
}

func (c *cron) Start() {
	for n, j := range jobs {
		name, job := n, j
		go func() {
			ticker := time.NewTicker(job.interval)
			for {
				<-ticker.C
				if err := job.fn(c.client); err != nil {
					log.Err(err).Str("name", name).Msg("error running job")
				}
			}
		}()

	}
}
