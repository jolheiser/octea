package cron

import (
	"context"
	"time"

	"github.com/google/go-github/v48/github"
	"github.com/rs/zerolog/log"
)

func init() {
	jobs["wait_merge"] = job{
		interval: time.Minute * 60,
		fn:       waitMerge,
	}
}

func waitMerge(client *github.Client) error {
	ctx := context.Background()

	issues, _, err := client.Issues.ListByRepo(ctx, "go-gitea", "gitea", &github.IssueListByRepoOptions{
		State:     "open",
		Labels:    []string{"reviewed/wait_merge"},
		Sort:      "created",
		Direction: "asc",
	})
	if err != nil {
		return err
	}

	if len(issues) < 1 {
		return nil
	}

	bases := make(map[string]int)
	for _, issue := range issues {
		if !issue.IsPullRequest() {
			log.Error().Int("index", issue.GetNumber()).Msg("encountered an issue with the wait_merge label")
			continue
		}
		pr, _, err := client.PullRequests.Get(ctx, "go-gitea", "gitea", issue.GetNumber())
		if err != nil {
			return err
		}

		baseRef := pr.GetBase().GetRef()
		count := bases[baseRef]
		if count >= 2 {
			continue
		}

		switch count {
		case 0:
			base, _, err := client.Repositories.GetCombinedStatus(ctx, "go-gitea", "gitea", baseRef, nil)
			if err != nil {
				return err
			}
			if base.GetState() == "pending" {
				log.Info().Msgf("skipping wait_merge while branch %q is pending", baseRef)
				continue
			}
			if !pr.GetMergeable() {
				if _, _, err := client.PullRequests.UpdateBranch(ctx, "go-gitea", "gitea", pr.GetNumber(), nil); err != nil {
					return err
				}
				// Increment twice because we need to wait for this one to merge before updating another for this ref
				bases[baseRef] += 2
				continue
			}
			if _, _, err := client.PullRequests.Merge(ctx, "go-gitea", "gitea", pr.GetNumber(), "", &github.PullRequestOptions{
				MergeMethod: "squash",
			}); err != nil {
				return err
			}
		case 1:
			if _, _, err := client.PullRequests.UpdateBranch(ctx, "go-gitea", "gitea", pr.GetNumber(), nil); err != nil {
				return err
			}
		}
		bases[baseRef]++
	}
	return nil
}
