module code.gitea.io/octea

go 1.18

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/google/go-github/v48 v48.2.0
	github.com/peterbourgon/ff/v3 v3.1.2
	github.com/rs/zerolog v1.26.1
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
)

require (
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	golang.org/x/crypto v0.0.0-20211215165025-cf75a172585e // indirect
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	google.golang.org/appengine v1.6.7 // indirect
)
