package events

import "github.com/google/go-github/v48/github"

type handlerError struct {
	Code    int
	Message string
}
type handler func(client *github.Client, event any) *handlerError

var Events map[string][]handler
