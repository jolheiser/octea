package events

import (
	"context"
	"net/http"
	"regexp"

	"github.com/google/go-github/v48/github"
	"github.com/rs/zerolog/log"
)

const pullRequest = "pull_request"

func init() {
	Events[pullRequest] = append(Events[pullRequest],
		removeHTMLComments,
	)
}

var commentRe = regexp.MustCompile(`<!--[\s\S]*?-->`)

func removeHTMLComments(client *github.Client, event any) *handlerError {
	pre := event.(github.PullRequestEvent)
	pr := pre.GetPullRequest()
	body := pr.GetBody()
	if !commentRe.MatchString(body) {
		return nil
	}
	body = commentRe.ReplaceAllString(body, "")
	pr.Body = &body
	if _, _, err := client.PullRequests.Edit(context.Background(), pre.GetRepo().GetOwner().GetName(), pre.GetRepo().GetName(), pr.GetNumber(), pr); err != nil {
		log.Err(err).Msg("could not edit pull request")
		return &handlerError{
			Code:    http.StatusInternalServerError,
			Message: "internal server error",
		}
	}
	return nil
}
