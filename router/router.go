package router

import (
	"encoding/json"
	"net/http"

	"code.gitea.io/octea/events"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/google/go-github/v48/github"
	"github.com/rs/zerolog/log"
)

type router struct {
	webhookSecret []byte
	client        *github.Client
}

func New(webhookSecret []byte, client *github.Client) *chi.Mux {
	m := chi.NewMux()
	m.Use(middleware.Logger)
	m.Use(middleware.Recoverer)

	middleware.DefaultLogger = middleware.RequestLogger(&middleware.DefaultLogFormatter{
		Logger: &log.Logger,
	})

	r := &router{
		webhookSecret: webhookSecret,
		client:        client,
	}
	m.Use(r.Verify)

	m.Post("/", r.handleEvents)

	return m
}

func (r *router) handleEvents(res http.ResponseWriter, req *http.Request) {
	eventType := req.Header.Get("X-GitHub-Event")

	var event any
	switch eventType {
	case "pull_request":
		event = github.PullRequestEvent{}
	}

	if err := json.NewDecoder(req.Body).Decode(&event); err != nil {
		http.Error(res, "invalid payload", http.StatusBadRequest)
		return
	}

	if handlers, ok := events.Events[eventType]; ok {
		for _, handler := range handlers {
			if err := handler(r.client, event); err != nil {
				http.Error(res, err.Message, err.Code)
				return
			}
		}
	}
}
