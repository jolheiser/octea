package router

import (
	"bytes"
	"io"
	"net/http"

	"github.com/google/go-github/v48/github"
)

func (r *router) Verify(next http.Handler) http.Handler {
	fn := func(res http.ResponseWriter, req *http.Request) {
		payload, err := github.ValidatePayload(req, r.webhookSecret)
		if err != nil {
			http.Error(res, "invalid payload", http.StatusUnauthorized)
		}
		req.Body = io.NopCloser(bytes.NewReader(payload))
		next.ServeHTTP(res, req)
	}

	return http.HandlerFunc(fn)
}
